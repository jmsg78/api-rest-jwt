const express = require("express");
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");

const app = express();

/*var corsOptions = {
  origin: "http://localhost:8081",
};

app.use(cors(corsOptions));
*/
const swaggerUserStore = require("./docs-store/user-swagger.json");
const swaggerEventStore = require("./docs-store/event-swagger.json");
// parse requests of content-type - application/json
app.use(express.json());
app.use("/api-docs/user", swaggerUi.serve, swaggerUi.setup(swaggerUserStore));
app.use("/api-docs/event", swaggerUi.serve, swaggerUi.setup(swaggerEventStore));
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Api Documentation." });
});

// set port, listen for requests
const PORT = process.env.PORT || 3007;
app.listen(PORT, () => {
  console.log(`Server Api doc is running on port ${PORT}.`);
});
