const express = require("express");
const cors = require("cors");
//const db = require("./models");

const app = express();

var corsOptions = {
  origin: "http://localhost:8081",
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
//db.sequelize.sync();
/*
db.sequelize.sync({ force: true }).then(() => {
  console.log("Drop and Resync Db");
  initial();
});
*/
// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Api Rest Auth JWT." });
});

// routes
require("./routes/auth.routes")(app);
require("./routes/user.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8082;
app.listen(PORT, () => {
  console.log(`Server Api Rest Auth is running on port ${PORT}.`);
});
