const db = require("../models");

const Event = db.events;
const { Op } = db.Sequelize;

// Create and Save a new Event
exports.create = (req, res) => {
  // Validate request

  if (
    !req.body.title ||
    !req.body.description ||
    !req.body.location ||
    !req.body.dateStart
  ) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  // Create a Event
  const event = {
    title: req.body.title,
    description: req.body.description,
    location: req.body.location,
    dateStart: req.body.dateStart,
    dateEnd: req.body.dateEnd ? req.body.dateEnd : null,
    email: req.body.email ? req.body.email : null,
    url: req.body.url ? req.body.url : null,
    image: req.body.image ? req.body.image : null,
    status: req.body.status ? req.body.status : "created",
    userId: req.body.userId,
  };

  // Save Event in the database
  Event.create(event)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err || "Some error occurred while creating the event.",
      });
    });
};

// Retrieve all Events from the database.
exports.findAll = (req, res) => {
  const { title } = req.query;
  const condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Event.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving events.",
      });
    });
};

// Find a single Event Id
exports.findOne = (req, res) => {
  const { id } = req.params;

  Event.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error retrieving event with id=${id}`,
      });
    });
};

// Find a single Event with an status
exports.findOneStatus = (req, res) => {
  const { status } = req.body;
  Event.findAll({ where: { status: status } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving events.",
      });
    });
};

// Update a Event by the id in the request
exports.update = (req, res) => {
  const { id } = req.params;

  Event.update(req.body, {
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Event was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update event with id=${id}. Maybe event was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Error updating event with id=${id}`,
      });
    });
};

// Delete a Event with the specified id in the request
exports.delete = (req, res) => {
  const { id } = req.params;

  Event.destroy({
    where: { id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Event was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete event with id=${id}. Maybe event was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: `Could not delete event with id=${id}`,
      });
    });
};

// Delete all Events from the database.
exports.deleteAll = (req, res) => {
  Event.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} events were deleted successfully!` });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all events.",
      });
    });
};

// Find all published Events
exports.findAllPublished = (req, res) => {
  Event.findAll({ where: { status: "published" } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving events.",
      });
    });
};
