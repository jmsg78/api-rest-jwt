const events = require("./event.controller.js");

const router = require("express").Router();

// Create a new Event
router.post("/", events.create);

// Retrieve all Events
router.get("/", events.findAll);

// Retrieve all published Events
router.get("/published", events.findAllPublished);

// Retrieve a single Events with id
router.get("/:id", events.findOne);

// Retrieve a single Event per status
router.post("/status", events.findOneStatus);

// Update a Events with id
router.put("/:id", events.update);

// Delete a Events with id
router.delete("/:id", events.delete);

// Create a new Events
router.delete("/", events.deleteAll);

module.exports = router;
